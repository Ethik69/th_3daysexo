import boto3


def lambda_handler(event, context):
    stack_name = event['stack_name']
    template_url = event['template_url']
    tag_name = event['tag_name']
    region = event['region']

    client = boto3.client('cloudformation', region_name=region)

    response = client.create_stack(
        StackName = stack_name,  # StateMachineStack
        TemplateURL = template_url,  # https://s3-eu-west-1.amazonaws.com/cfn-stacks-3days-th/StateMachine.json
        TimeoutInMinutes = 123,
        ResourceTypes = ['AWS::*'],
        OnFailure = 'ROLLBACK',
        Tags = [{'Key': 'Name','Value': tag_name}]  # StateMachineStack
    )
