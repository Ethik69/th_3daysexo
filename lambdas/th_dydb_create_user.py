import json
import uuid
import boto3
import hashlib
from boto3.dynamodb.conditions import Key, Attr


def lambda_handler(event, context):
    # Get data from event
    user_name = event['user_name']
    salt = 'th'
    password = event['password'] + salt

    # Get the password hash
    password = hashlib.sha512(password.encode())
    organisation_name = event['organisation']

    # Create DynamoDB resource
    user_db_name = 'th_users'
    user_db_table = boto3.resource('dynamodb').Table(user_db_name)

    # Check if the user already exist
    user = user_db_table.query(IndexName='user_name',
                               KeyConditionExpression=Key('user_name').eq(user_name)
                              )

    if user['Count'] != 0:
        return {'msg': 'user already exist'}

    # Check if the organisation already exist
    orga = user_db_table.query(IndexName='organisation',
                               KeyConditionExpression=Key('organisation').eq(organisation_name)
                              )

    if orga['Count'] != 0:
        return {'msg': 'organisation already exist'}

    # Create UserId
    id = uuid.uuid4()

    # Add User in DB
    user_db_table.put_item(
            Item={'id': str(id),
                  'user_name': user_name,
                  'password': password.hexdigest(),
                  'organisation': organisation_name,
                  'activate': 'True',
                  'account_status': 'enabled',
                  'request_count': 0
                  })

    return {'msg': 'user and orga db created'}
