import boto3
from boto3.dynamodb.conditions import Key, Attr


def lambda_handler(event, context):
    # Raise an exception based on chosen status code
    user_name = event['user_name']

    # Raise an exception based on chosen status code
    user_db_table = boto3.resource('dynamodb').Table('th_users')

    # Check if the user already exist
    user = user_db_table.query(IndexName='user_name',
                               KeyConditionExpression=Key('user_name').eq(user_name)
                              )

    if user['Count'] != 0:
        user_id = user['Items'][0]['id']
        user_activation = user['Items'][0]['activate']
        user_account_status = user['Items'][0]['account_status']
    else:
        return {'msg': 'User Does Not Exist'}

    if user_account_status == "disable" or not user_activation:
        return {'status': 'stop'}
    else:
        return {'status': 'continue'}
