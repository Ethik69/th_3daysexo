import json
import boto3
from boto3.dynamodb.conditions import Key, Attr


def lambda_handler(event, context):
    # Raise an exception based on chosen status code
    user_name = event['user_name']

    # Raise an exception based on chosen status code
    user_db_table = boto3.resource('dynamodb').Table('th_users')

    # Check if the user already exist
    user = user_db_table.query(IndexName='user_name',
                               KeyConditionExpression=Key('user_name').eq(user_name)
                              )

    if user['Count'] != 0:
        user_id = user['Items'][0]['id']
        user_request_count = user['Items'][0]['request_count']
    else:
        return {'msg': 'User Does Not Exist'}

    user_request_count = user_request_count + 1

    # Update user request count
    user_db_table.update_item(Key={'id': user_id},
                              AttributeUpdates={
                              'request_count': {'Value': user_request_count},
                              })

    return {'msg': 'User Request Count Updated'}
