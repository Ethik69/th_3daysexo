import json
import time
import boto3


def lambda_handler(event, context):
    # Get data from event
    analyse_data = event[0]['analyse_data']
    status_code = event[0]['data']['status']
    body = event[0]['data']['body']


    if body == analyse_data['body'] and status_code == analyse_data['status_code']:
        return {'send': True, 'subject': 'Same notice',
                'message': 'Body or Status Code Does No Match'}

    return {'send': False}
