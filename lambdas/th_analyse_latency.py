import json
import time
import boto3


def lambda_handler(event, context):
    # Get data from event
    wanted_time = int(event[0]['analyse_data']['time'])
    latency = event[0]['data']['latency']
    status_code = event[0]['data']['status']

    if latency > wanted_time:
        return {'send': True, 'subject': 'Latency notice',
                'message': 'Latency above %sms' % wanted_time}

    elif status_code == 400:
        return {'send': True, 'subject': 'Error Code notice',
                'message': 'Error code: %s, Client error' % status_code}

    elif status_code == 500:
        return {'send': True, 'subject': 'Error Code notice',
                'message': 'Error code: %s, Server error' % status_code}

    return {'send': False}
