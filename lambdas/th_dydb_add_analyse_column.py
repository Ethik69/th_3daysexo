import json
import uuid
import boto3
import hashlib
from boto3.dynamodb.conditions import Key, Attr


def lambda_handler(event, context):
    # Get data from event
    user_name = event['user_name']
    stat_name = 'stats_' + event['stat_name']

    # Create DynamoDB resource
    user_db_name = 'th_users'
    user_db_table = boto3.resource('dynamodb').Table(user_db_name)

    # Check if the user already exist
    user = user_db_table.query(IndexName='user_name',
                               KeyConditionExpression=Key('user_name').eq(user_name)
                              )

    if user['Count'] != 0:
        user_id = user['Items'][0]['id']

    # Add User in DB
    user_db_table.update_item(Key={'id': user_id},
                              AttributeUpdates={
                              stat_name: {'Value': "empty"},
                              })

    return {'msg': 'user and orga db created'}
