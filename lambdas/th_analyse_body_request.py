import json
import time
import boto3


def lambda_handler(event, context):
    # Get data from event
    analyse_data = event[0]['analyse_data']
    status_code = event[0]['data']['status']
    latency = event[0]['data']['latency']

    # Need some improvement... check the first number in latency and time work but is awful

    # Check if status code or latency are the same or not than the given data
    if analyse_data['status_code'] != status_code or int(str(latency)[0]) != int(str(analyse_data['time'])[0]):
        return {'send': True, 'subject': 'Bod notice',
                'message': 'Status code or Body Does Not Match'}

    return {'send': False}
