import time
import random


def lambda_handler(event, context):
    # Wait for x seconds
    rand = random.randint(0, 5)
    time.sleep(rand)

    # Get a status code randomly
    status_code = random.choice([200, 300, 400, 500])

    # Raise an exception based on chosen status code
    if status_code == 200:
        raise Exception('Normal Request: This Is The Output When Everything Is Working.')
    elif status_code == 300:
        raise Exception('Redirection: You\'v Been Redirected.')
    elif status_code == 400:
        raise Exception('Bad Request: You\'r trying to do an invalid request.')
    elif status_code == 500:
        raise Exception('Server Error: I Think That The Server Is In Bad Situation.')
    else:
        return "Fail"
