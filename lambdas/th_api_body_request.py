import time
import json


def lambda_handler(event, context):
    # Get data from event
    status_code = event['status_code']

    # Sleep for the give time
    time.sleep(event['time'] / 1000)

    # Raise an exception based on given status code
    if status_code == 200:
        raise Exception('Normal Request: This Is The Output When Everything Is Working.')
    elif status_code == 300:
        raise Exception('Redirection: You\'v Been Redirected.')
    elif status_code == 400:
        raise Exception('Bad Request: You\'r trying to do an invalid request.')
    elif status_code == 500:
        raise Exception('Server Error: I Think That The Server Is In Bad Situation.')
    else:
        return "Fail"
