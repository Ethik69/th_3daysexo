import boto3
import json


def lambda_handler(event, context):
    api_region = event['api_region']

    # Create apigateway/sns clients
    client_api = boto3.client('apigateway', region_name=api_region)
    client_sns = boto3.client('sns')

    # Get data from event
    api_prefix = event['api_prefix']
    stage = event['stage']
    method = event['method']
    resource_id = event['resource_id']
    headers = {'Content-type': 'application/json'}
    body = json.dumps(event['body'])

    # Format url
    url = 'https://%s.execute-api.%s.amazonaws.com/%s' % (api_prefix,
                                                          api_region,
                                                          stage)

    # Send the request to the given api/route
    # Can be easily changed by requests module to by used with non aws apis =)
    response_invoke = client_api.test_invoke_method(restApiId=api_prefix,
                                                    resourceId=resource_id,
                                                    httpMethod=method,
                                                    body=body,
                                                    headers=headers,
                                                    stageVariables={stage: '/%s' % stage}
                                                    )

    # Add wanted analyse to response_invoke
    # Useless but i'm lazy...... right now don't want to change every call to this i made...
    response_invoke['wanted_analyse'] = event['wanted_analyse']

    return response_invoke
