import json
import time
import boto3
from boto3.dynamodb.conditions import Key, Attr


def lambda_handler(event, context):
    # Get data from event
    user_name = event[0]['user_name']
    stat_name = event[0]['data']['wanted_analyse']
    latency = event[0]['data']['latency']

    # Create DynamoDB resource
    user_db_table = boto3.resource('dynamodb').Table('th_users')

    # Check if the user already exist
    user = user_db_table.query(IndexName='user_name',
                               KeyConditionExpression=Key('user_name').eq(user_name)
                              )

    if user['Count'] != 0:
        user_id = user['Items'][0]['id']
        user_stats = user['Items'][0]["stats_%s" % stat_name]
    else:
        return {'msg': 'User Does Not Exist'}

    # Format data
    formated_data = "%s_%s" % (latency, time.strftime('%H:%M:%S-%d/%m/%Y', time.localtime()))

    if user_stats == "empty":
        user_stats = formated_data
    else:
        user_stats = "%s;%s" % (user_stats, formated_data)


    # Update user stats
    user_db_table.update_item(Key={'id': user_id},
                              AttributeUpdates={
                              'stats_latency': {'Value': user_stats},
                              })

    return {'msg': 'User Stats Updated'}
