import boto3


def lambda_handler(event, context):
    # Get data from event
    topic_arn = event[0]['topic_arn']  # From event/db ??
    subject = event[0]['notification']['subject']
    message = event[0]['notification']['message']

    # Create sns client
    client_sns = boto3.client('sns')

    # Send notification to given topic
    response = client_sns.publish(TopicArn=topic_arn,
                                  Subject=subject,
                                  Message=message,
                                  MessageStructure='raw'
                                  )

    return {'msg': 'Notifications sent', 'data': response}
