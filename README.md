# API Monitoring

One API to monitor them all !

Powered by [AWS](https://aws.amazon.com/fr/)

## Stack CloudFormation

CloudFormation provides a common language for you to describe and provision all the infrastructure resources in your cloud environment.

- `Event`: CloudWatch Event stack for cron task like process.
- `SNSTopic`: SNS Topic for communication channels.
- `StateMachine`: Step Function AWS services coordination.(Deploy Lambdas and State Machine)

## Lambdas

#### Analyses

- `th_analyse_same.py`: Check if the API return the predicted output.
- `th_analyse_latency.py`: Check if the latency if above chosen time.
- `th_analyse_body_request.py`: Check if the API return the given data in the given time.

#### Data Base

- `th_dydb_create_user.py`: Add a new user in the Data Base.
- `th_dydb_update_stats.py`: Update given stats in the Data Base.
- `th_dydb_check_activation.py`: Check if the user account is enabled/disabled and if monitoring if turned on/off.
- `th_dydb_add_analyse_column.py`: Add a new column stats_* in the Data Base.
- `th_dydb_update_request_count.py`: Increment the request count in the Data Base.

#### API

- `th_api_same_get.py`: Always return the same output.
- `th_api_same_post.py`: Always return the same output.
- `th_api_temp_alea.py`: Wait x (random) seconds, then return random status code.
- `th_api_body_request.py`: Wait x (given) seconds, then return given status code.

#### Other

- `th_request.py`: Make a call on the given api on the given route.
- `th_sns_send_notif.py`: Send a notification on the given topic.
- `th_launch_cfn_stack.py`: Launch the given stack CloudFormation in the given area.

## DynamoDB

![DynamoDB Image](dtndb_db_schem.png)

## Test API

![Test API Image](Diagram_API_test.png)


## Step Function

![Step Function Image](Diagram_step_function.jpg)
